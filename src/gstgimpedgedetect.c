/* GStreamer
 * Copyright (C) 2016 FIXME <fixme@example.com>
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Library General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Library General Public License for more details.
 *
 * You should have received a copy of the GNU Library General Public
 * License along with this library; if not, write to the
 * Free Software Foundation, Inc., 51 Franklin Street, Suite 500,
 * Boston, MA 02110-1335, USA.
 */
/**
 * SECTION:element-gstgimpedgedetect
 *
 * The gimpedgedetect element does FIXME stuff.
 *
 * <refsect2>
 * <title>Example launch line</title>
 * |[
 * gst-launch-1.5 -v videotestsrc ! gimpedgedetect ! videoconvert ! autovideosink
 * ]|
 * FIXME Describe what the pipeline does.
 * </refsect2>
 */

#ifdef HAVE_CONFIG_H
#include "config.h"
#endif

#include <gst/gst.h>
#include <gst/video/video.h>
#include <gst/video/gstvideofilter.h>
#include "gstgimpedgedetect.h"

GST_DEBUG_CATEGORY_STATIC (gst_gimpedgedetect_debug_category);
#define GST_CAT_DEFAULT gst_gimpedgedetect_debug_category

/* prototypes */


static void gst_gimpedgedetect_set_property (GObject * object,
    guint property_id, const GValue * value, GParamSpec * pspec);
static void gst_gimpedgedetect_get_property (GObject * object,
    guint property_id, GValue * value, GParamSpec * pspec);
static void gst_gimpedgedetect_dispose (GObject * object);
static void gst_gimpedgedetect_finalize (GObject * object);

static gboolean gst_gimpedgedetect_start (GstBaseTransform * trans);
static gboolean gst_gimpedgedetect_stop (GstBaseTransform * trans);
static gboolean gst_gimpedgedetect_set_info (GstVideoFilter * filter, GstCaps * incaps,
    GstVideoInfo * in_info, GstCaps * outcaps, GstVideoInfo * out_info);
static GstFlowReturn gst_gimpedgedetect_transform_frame (GstVideoFilter * filter,
    GstVideoFrame * inframe, GstVideoFrame * outframe);
static GstFlowReturn gst_gimpedgedetect_transform_frame_ip (GstVideoFilter * filter,
    GstVideoFrame * frame);

enum
{
  PROP_0
};


/* ***********************   Edge Detection   ******************** */

enum
{
  SOBEL,
  PREWITT,
  GRADIENT,
  ROBERTS,
  DIFFERENTIAL,
  LAPLACE
};

typedef struct
{
  gdouble  amount;
  gint     edgemode;
  gint     wrapmode;
} EdgeVals;


/*
 * Function prototypes.
 */

static void       edge                (guint32 *src, guint32 *dest, gint map_width, gint map_height, gint sstride, gint dstride);
static gint       edge_detect         (const guchar     *data);
static gint       prewitt             (const guchar     *data);
static gint       gradient            (const guchar     *data);
static gint       roberts             (const guchar     *data);
static gint       differential        (const guchar     *data);
static gint       laplace             (const guchar     *data);
static gint       sobel               (const guchar     *data);

/***** Local vars *****/

static EdgeVals evals =
{
  1.0,                           /* amount */
  SOBEL							 /* SOBEL - Edge detection algorithm */
};

/* pad templates */

/* FIXME: add/remove formats you can handle */
#define VIDEO_SRC_CAPS \
    GST_VIDEO_CAPS_MAKE("{ RGBx, BGRx }")

/* FIXME: add/remove formats you can handle */
#define VIDEO_SINK_CAPS \
    GST_VIDEO_CAPS_MAKE("{ RGBx, BGRx }")


#if G_BYTE_ORDER == G_LITTLE_ENDIAN
#define CAPS_STR GST_VIDEO_CAPS_MAKE ("{  BGRx, RGBx }")
#else
#define CAPS_STR GST_VIDEO_CAPS_MAKE ("{  xBGR, xRGB }")
#endif

/* class initialization */

G_DEFINE_TYPE_WITH_CODE (GstGimpedgedetect, gst_gimpedgedetect, GST_TYPE_VIDEO_FILTER,
  GST_DEBUG_CATEGORY_INIT (gst_gimpedgedetect_debug_category, "gimpedgedetect", 0,
  "debug category for gimpedgedetect element"));

static void
gst_gimpedgedetect_class_init (GstGimpedgedetectClass * klass)
{
  GObjectClass *gobject_class = G_OBJECT_CLASS (klass);
  GstBaseTransformClass *base_transform_class = GST_BASE_TRANSFORM_CLASS (klass);
  GstVideoFilterClass *video_filter_class = GST_VIDEO_FILTER_CLASS (klass);

  /* Setting up pads and setting metadata should be moved to
     base_class_init if you intend to subclass this class. */
  gst_element_class_add_pad_template (GST_ELEMENT_CLASS(klass),
      gst_pad_template_new ("src", GST_PAD_SRC, GST_PAD_ALWAYS,
        gst_caps_from_string (VIDEO_SRC_CAPS)));
  gst_element_class_add_pad_template (GST_ELEMENT_CLASS(klass),
      gst_pad_template_new ("sink", GST_PAD_SINK, GST_PAD_ALWAYS,
        gst_caps_from_string (VIDEO_SINK_CAPS)));

  gst_element_class_set_static_metadata (GST_ELEMENT_CLASS(klass),
      "GIMP edge detect", "Generic", "FIXME Description",
      "FIXME <fixme@example.com>");

  gobject_class->set_property = gst_gimpedgedetect_set_property;
  gobject_class->get_property = gst_gimpedgedetect_get_property;
  gobject_class->dispose = gst_gimpedgedetect_dispose;
  gobject_class->finalize = gst_gimpedgedetect_finalize;
  base_transform_class->start = GST_DEBUG_FUNCPTR (gst_gimpedgedetect_start);
  base_transform_class->stop = GST_DEBUG_FUNCPTR (gst_gimpedgedetect_stop);
  video_filter_class->set_info = GST_DEBUG_FUNCPTR (gst_gimpedgedetect_set_info);
  video_filter_class->transform_frame = GST_DEBUG_FUNCPTR (gst_gimpedgedetect_transform_frame);
  video_filter_class->transform_frame_ip = GST_DEBUG_FUNCPTR (gst_gimpedgedetect_transform_frame_ip);

}

static void
gst_gimpedgedetect_init (GstGimpedgedetect *gimpedgedetect)
{
}

void
gst_gimpedgedetect_set_property (GObject * object, guint property_id,
    const GValue * value, GParamSpec * pspec)
{
  GstGimpedgedetect *gimpedgedetect = GST_GIMPEDGEDETECT (object);

  GST_DEBUG_OBJECT (gimpedgedetect, "set_property");

  switch (property_id) {
    default:
      G_OBJECT_WARN_INVALID_PROPERTY_ID (object, property_id, pspec);
      break;
  }
}

void
gst_gimpedgedetect_get_property (GObject * object, guint property_id,
    GValue * value, GParamSpec * pspec)
{
  GstGimpedgedetect *gimpedgedetect = GST_GIMPEDGEDETECT (object);

  GST_DEBUG_OBJECT (gimpedgedetect, "get_property");

  switch (property_id) {
    default:
      G_OBJECT_WARN_INVALID_PROPERTY_ID (object, property_id, pspec);
      break;
  }
}

void
gst_gimpedgedetect_dispose (GObject * object)
{
  GstGimpedgedetect *gimpedgedetect = GST_GIMPEDGEDETECT (object);

  GST_DEBUG_OBJECT (gimpedgedetect, "dispose");

  /* clean up as possible.  may be called multiple times */

  G_OBJECT_CLASS (gst_gimpedgedetect_parent_class)->dispose (object);
}

void
gst_gimpedgedetect_finalize (GObject * object)
{
  GstGimpedgedetect *gimpedgedetect = GST_GIMPEDGEDETECT (object);

  GST_DEBUG_OBJECT (gimpedgedetect, "finalize");

  /* clean up object here */

  G_OBJECT_CLASS (gst_gimpedgedetect_parent_class)->finalize (object);
}

static gboolean
gst_gimpedgedetect_start (GstBaseTransform * trans)
{
  GstGimpedgedetect *gimpedgedetect = GST_GIMPEDGEDETECT (trans);

  GST_DEBUG_OBJECT (gimpedgedetect, "start");

  return TRUE;
}

static gboolean
gst_gimpedgedetect_stop (GstBaseTransform * trans)
{
  GstGimpedgedetect *gimpedgedetect = GST_GIMPEDGEDETECT (trans);

  GST_DEBUG_OBJECT (gimpedgedetect, "stop");

  return TRUE;
}

static gboolean
gst_gimpedgedetect_set_info (GstVideoFilter * filter, GstCaps * incaps,
    GstVideoInfo * in_info, GstCaps * outcaps, GstVideoInfo * out_info)
{
  GstGimpedgedetect *gimpedgedetect = GST_GIMPEDGEDETECT (filter);

  GST_DEBUG_OBJECT (gimpedgedetect, "set_info");

  return TRUE;
}

/* transform */
static GstFlowReturn
gst_gimpedgedetect_transform_frame (GstVideoFilter * filter, GstVideoFrame * inframe,
    GstVideoFrame * outframe)
{
	GstGimpedgedetect *gimpedgedetect = GST_GIMPEDGEDETECT (filter);

	GST_DEBUG_OBJECT (gimpedgedetect, "transform_frame");

	guint32 *src, *dest;
	gint width, height, sstride, dstride;

	src = GST_VIDEO_FRAME_PLANE_DATA (inframe, 0);
	dest = GST_VIDEO_FRAME_PLANE_DATA (outframe, 0);

	height = GST_VIDEO_FRAME_HEIGHT (inframe);
	width = GST_VIDEO_FRAME_WIDTH (inframe);

	sstride = GST_VIDEO_FRAME_PLANE_STRIDE (inframe, 0);
	dstride = GST_VIDEO_FRAME_PLANE_STRIDE (outframe, 0);

	// render
	edge(src, dest, width, height, sstride / 4, dstride / 4);

	return GST_FLOW_OK;
}

static GstFlowReturn
gst_gimpedgedetect_transform_frame_ip (GstVideoFilter * filter, GstVideoFrame * frame)
{
  GstGimpedgedetect *gimpedgedetect = GST_GIMPEDGEDETECT (filter);

  GST_DEBUG_OBJECT (gimpedgedetect, "transform_frame_ip");

  return GST_FLOW_OK;
}

static gboolean
plugin_init (GstPlugin * plugin)
{

  /* FIXME Remember to set the rank if it's an element that is meant
     to be autoplugged by decodebin. */
  return gst_element_register (plugin, "gimpedgedetect", GST_RANK_NONE,
      GST_TYPE_GIMPEDGEDETECT);
}

/* ***********************   Edge Detection Main   **************** */


static void
edge(guint32 *source, guint32 *orig_destination, gint map_width, gint map_height, gint sstride, gint dstride)
{
	gint              x, y;
	gint 			  bpp = 4;
	gint 		      filter_width = map_width - 4;
	gint 		      filter_height = map_height - 4;

	guint32 *destination = orig_destination;

	if (evals.amount < 1.0)
		evals.amount = 1.0;

	const gint sxjump = 4 * bpp;

	// main image processing
	source += 4 * sstride;
	destination += 4 * dstride;
	for (y = 5;
		y < filter_height;
		y++, source += sstride, destination += dstride)
	{
		guchar *src = (guchar *)source + sxjump;
		guchar *dest = (guchar *)destination + sxjump;

		for (x = 5;
			x < filter_width;
			x++,  src += bpp, dest += bpp)
		{
			/* get the 3x3 kernel into a guchar array,
			 * and send it to edge_detect */
			guchar kernel[9];

#define PIX(X,Y)  src[ (Y-1)*(int)sstride*4 + (X-1)*(int)bpp + 0 ]
			/* make convolution */
			kernel[3*0 + 0] = PIX(0,0);
			kernel[3*0 + 1] = PIX(0,1);
			kernel[3*0 + 2] = PIX(0,2);
			kernel[3*1 + 0] = PIX(1,0);
			kernel[3*1 + 1] = PIX(1,1);
			kernel[3*1 + 2] = PIX(1,2);
			kernel[3*2 + 0] = PIX(2,0);
			kernel[3*2 + 1] = PIX(2,1);
			kernel[3*2 + 2] = PIX(2,2);

#undef  PIX

			dest[0] = sobel (kernel);

#define PIX(X,Y)  src[ (Y-1)*(int)sstride*4 + (X-1)*(int)bpp + 1 ]
			/* make convolution */
			kernel[3*0 + 0] = PIX(0,0);
			kernel[3*0 + 1] = PIX(0,1);
			kernel[3*0 + 2] = PIX(0,2);
			kernel[3*1 + 0] = PIX(1,0);
			kernel[3*1 + 1] = PIX(1,1);
			kernel[3*1 + 2] = PIX(1,2);
			kernel[3*2 + 0] = PIX(2,0);
			kernel[3*2 + 1] = PIX(2,1);
			kernel[3*2 + 2] = PIX(2,2);

#undef  PIX

			dest[1] = sobel (kernel);

#define PIX(X,Y)  src[ (Y-1)*(int)sstride*4 + (X-1)*(int)bpp + 2 ]
			/* make convolution */
			kernel[3*0 + 0] = PIX(0,0);
			kernel[3*0 + 1] = PIX(0,1);
			kernel[3*0 + 2] = PIX(0,2);
			kernel[3*1 + 0] = PIX(1,0);
			kernel[3*1 + 1] = PIX(1,1);
			kernel[3*1 + 2] = PIX(1,2);
			kernel[3*2 + 0] = PIX(2,0);
			kernel[3*2 + 1] = PIX(2,1);
			kernel[3*2 + 2] = PIX(2,2);

#undef  PIX

			dest[2] = sobel (kernel);

#define PIX(X,Y)  src[ (Y-1)*(int)sstride*4 + (X-1)*(int)bpp + 3 ]
			/* make convolution */
			kernel[3*0 + 0] = PIX(0,0);
			kernel[3*0 + 1] = PIX(0,1);
			kernel[3*0 + 2] = PIX(0,2);
			kernel[3*1 + 0] = PIX(1,0);
			kernel[3*1 + 1] = PIX(1,1);
			kernel[3*1 + 2] = PIX(1,2);
			kernel[3*2 + 0] = PIX(2,0);
			kernel[3*2 + 1] = PIX(2,1);
			kernel[3*2 + 2] = PIX(2,2);

#undef  PIX

			dest[3] = sobel (kernel);
		}
	}

	// edge cases - top
	destination = orig_destination;
	for (y = 0;
		y < 5;
		y++, destination += dstride)
	{
		guchar *dest = (guchar *)destination;

		for (x = 0;
			x < map_width;
			x++, dest += bpp)
		{
			dest[0] = 0;
			dest[1] = 0;
			dest[2] = 0;
			dest[3] = 0;
		}
	}

	// edge cases - bottom
	destination = orig_destination + filter_height * sstride;
	for (y = 0;
		y < 5;
		y++, destination += dstride)
	{
		guchar *dest = (guchar *)destination;

		for (x = 0;
			x < map_width;
			x++, dest += bpp)
		{
			dest[0] = 0;
			dest[1] = 0;
			dest[2] = 0;
			dest[3] = 0;
		}
	}

	// edge cases - left
	destination = orig_destination + 4 * dstride;
	for (y = 5;
		y < filter_height - 4;
		y++, destination += dstride)
	{
		guchar *dest = (guchar *)destination;

		for (x = 0;
			x < 5;
			x++, dest += bpp)
		{
			dest[0] = 0;
			dest[1] = 0;
			dest[2] = 0;
			dest[3] = 0;
		}
	}

	// edge cases - right
	destination = orig_destination + 4 * dstride + filter_width * bpp;
	for (y = 5;
		y < filter_height - 4;
		y++, destination += dstride)
	{
		guchar *dest = (guchar *)destination;

		for (x = 0;
			x < 5;
			x++, dest += bpp)
		{
			dest[0] = 0;
			dest[1] = 0;
			dest[2] = 0;
			dest[3] = 0;
		}
	}

}

static guchar
_clamp0255(gint x)
{
	if (x < 0)
		return 0;
	else if (x > 255)
		return 255;
	else
		return x;
}

/*
 * Edge detect switcher function
 */

static gint
edge_detect (const guchar *data)
{
  gint ret;

  switch (evals.edgemode)
    {
    case SOBEL:
      ret = sobel (data);
      break;
    case PREWITT:
      ret = prewitt (data);
      break;
    case GRADIENT:
      ret = gradient (data);
      break;
    case ROBERTS:
      ret = roberts (data);
      break;
    case DIFFERENTIAL:
      ret = differential (data);
      break;
    case LAPLACE:
      ret = laplace (data);
      break;
    default:
      ret = -1;
      break;
    }

  // TODO: CLAMP0255
  return _clamp0255(ret);
}


/*
 * Sobel Edge detector
 */
static gint
inline sobel (const guchar *data)
{
  const gint v_kernel[9] = { -1,  0,  1,
                             -2,  0,  2,
                             -1,  0,  1 };
  const gint h_kernel[9] = { -1, -2, -1,
                              0,  0,  0,
                              1,  2,  1 };

  gint v_grad = 0, h_grad = 0;

  v_grad += v_kernel[0] * data[0];
  h_grad += h_kernel[0] * data[0];
  v_grad += v_kernel[1] * data[1];
  h_grad += h_kernel[1] * data[1];
  v_grad += v_kernel[2] * data[2];
  h_grad += h_kernel[2] * data[2];
  v_grad += v_kernel[3] * data[3];
  h_grad += h_kernel[3] * data[3];
  v_grad += v_kernel[4] * data[4];
  h_grad += h_kernel[4] * data[4];
  v_grad += v_kernel[5] * data[5];
  h_grad += h_kernel[5] * data[5];
  v_grad += v_kernel[6] * data[6];
  h_grad += h_kernel[6] * data[6];
  v_grad += v_kernel[7] * data[7];
  h_grad += h_kernel[7] * data[7];
  v_grad += v_kernel[8] * data[8];
  h_grad += h_kernel[8] * data[8];

  return _clamp0255 ( sqrt (v_grad * v_grad * evals.amount +
               h_grad * h_grad * evals.amount) );
}

/*
 * Edge detector via template matting
 *   -- Prewitt Compass
 */
static gint
prewitt (const guchar *data)
{
  gint k, max;
  gint m[8];

  m[0] =   data [0] +   data [1] + data [2]
         + data [3] - 2*data [4] + data [5]
         - data [6] -   data [7] - data [8];
  m[1] =   data [0] +   data [1] + data [2]
         + data [3] - 2*data [4] - data [5]
         + data [6] -   data [7] - data [8];
  m[2] =   data [0] +   data [1] - data [2]
         + data [3] - 2*data [4] - data [5]
         + data [6] +   data [7] - data [8];
  m[3] =   data [0] -   data [1] - data [2]
         + data [3] - 2*data [4] - data [5]
         + data [6] +   data [7] + data [8];
  m[4] = - data [0] -   data [1] - data [2]
         + data [3] - 2*data [4] + data [5]
         + data [6] +   data [7] + data [8];
  m[5] = - data [0] -   data [1] + data [2]
         - data [3] - 2*data [4] + data [5]
         + data [6] +   data [7] + data [8];
  m[6] = - data [0] +   data [1] + data [2]
         - data [3] - 2*data [4] + data [5]
         - data [6] +   data [7] + data [8];
  m[7] =   data [0] +   data [1] + data [2]
         - data [3] - 2*data [4] + data [5]
         - data [6] -   data [7] + data [8];

  for (k = 0, max = 0; k < 8; k++)
    if (max < m[k])
      max = m[k];

  return evals.amount * max;
}

/*
 * Gradient Edge detector
 */
static gint
gradient (const guchar *data)
{
  const gint v_kernel[9] = { 0,  0,  0,
                             0,  4, -4,
                             0,  0,  0 };
  const gint h_kernel[9] = { 0,  0,  0,
                             0, -4,  0,
                             0,  4,  0 };

  gint i;
  gint v_grad, h_grad;

  for (i = 0, v_grad = 0, h_grad = 0; i < 9; i++)
    {
      v_grad += v_kernel[i] * data[i];
      h_grad += h_kernel[i] * data[i];
    }

  return  sqrt (v_grad * v_grad * evals.amount +
                h_grad * h_grad * evals.amount);
}

/*
 * Roberts Edge detector
 */
static gint
roberts (const guchar *data)
{
  const gint v_kernel[9] = { 0,  0,  0,
                             0,  4,  0,
                             0,  0, -4 };
  const gint h_kernel[9] = { 0,  0,  0,
                             0,  0,  4,
                             0, -4,  0 };
  gint i;
  gint v_grad, h_grad;

  for (i = 0, v_grad = 0, h_grad = 0; i < 9; i++)
    {
      v_grad += v_kernel[i] * data[i];
      h_grad += h_kernel[i] * data[i];
    }

  return sqrt (v_grad * v_grad * evals.amount +
               h_grad * h_grad * evals.amount);
}

/*
 * Differential Edge detector
 */
static gint
differential (const guchar *data)
{
  const gint v_kernel[9] = { 0,  0,  0,
                             0,  2, -2,
                             0,  2, -2 };
  const gint h_kernel[9] = { 0,  0,  0,
                             0, -2, -2,
                             0,  2,  2 };
  gint i;
  gint v_grad, h_grad;

  for (i = 0, v_grad = 0, h_grad = 0; i < 9; i++)
    {
      v_grad += v_kernel[i] * data[i];
      h_grad += h_kernel[i] * data[i];
    }

  return sqrt (v_grad * v_grad * evals.amount +
               h_grad * h_grad * evals.amount);
}

/*
 * Laplace Edge detector
 */
static gint
laplace (const guchar *data)
{
  const gint kernel[9] = { 1,  1,  1,
                           1, -8,  1,
                           1,  1,  1 };
  gint i;
  gint grad;

  for (i = 0, grad = 0; i < 9; i++)
    grad += kernel[i] * data[i];

  return grad * evals.amount;
}


/* FIXME: these are normally defined by the GStreamer build system.
   If you are creating an element to be included in gst-plugins-*,
   remove these, as they're always defined.  Otherwise, edit as
   appropriate for your external plugin package. */
#ifndef VERSION
#define VERSION "0.0.FIXME"
#endif
#ifndef PACKAGE
#define PACKAGE "FIXME_package"
#endif
#ifndef PACKAGE_NAME
#define PACKAGE_NAME "FIXME_package_name"
#endif
#ifndef GST_PACKAGE_ORIGIN
#define GST_PACKAGE_ORIGIN "http://FIXME.org/"
#endif

GST_PLUGIN_DEFINE (GST_VERSION_MAJOR,
    GST_VERSION_MINOR,
    gimpedgedetect,
    "FIXME plugin description",
    plugin_init, VERSION, "LGPL", PACKAGE_NAME, GST_PACKAGE_ORIGIN)

